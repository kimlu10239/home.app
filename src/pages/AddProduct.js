import React,{useState,useEffect,useContext} from 'react';
import {Form, Button} from 'react-bootstrap';

import Swal from 'sweetalert2'

import UserContext from '../userContext';

import {Redirect,useHistory} from 'react-router-dom';

export default function AddProduct(){

	const{user} = useContext(UserContext);

	const history = useHistory();

	const [name,setName] = useState("");
	const [description,setDescription] = useState("");
	const [price,setPrice] = useState("");
	const [category,setCategory] = useState("");
	const [stock,setStock] = useState("");

	const [isActive,setIsActive] = useState(false);

	useEffect(()=>{
		if(name !== "" && description !== "" && price !== "" && category !== "" && stock !== ""){
			setIsActive(true)
		} else{
			setIsActive(false)
		}

	},[name,description,price,category,stock])

	function createProduct(e){
		e.preventDefault();

		fetch('https://immense-brushlands-59734.herokuapp.com/products/create',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				category: category,
				stock: stock,
				price: price 
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data.name){
				Swal.fire({
					icon: "success",
					title: `Product: ${data.name} Successfully Added!`,
					text: `You will be redirected back to the products dashboard.`
				})

				history.push('/products')
			} else {
				Swal.fire({
					icon: "error",
					title: "Failed to add product.",
					text: data.message
				})
			}	
		})

		setName("");
		setDescription("");
		setPrice(0);
		setStock(0);
		//setCategory("");

	}

	/*
		<Form.Group>
			<Form.Label>Category:</Form.Label>
			<Form.Control type="text" value={category} onChange={e => {setCategory(e.target.value)}} required/>
		</Form.Group>
	*/

	return (
		user.isAdmin
		? 
			<>
				<h1 className="my-5 text-center">Add Product</h1>
				<Form onSubmit={e => createProduct(e)}>
					<Form.Group>
						<Form.Label>Product Name:</Form.Label>
						<Form.Control type="text" value={name} onChange={e => {setName(e.target.value)}} placeholder="Enter Product Name" required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Description:</Form.Label>
						<Form.Control type="text" value={description} onChange={e => {setDescription(e.target.value)}} placeholder="Enter Description" required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Category:</Form.Label>
						<Form.Control as="select" value={category} onChange={e => {setCategory(e.target.value)}} required>
							<option>Tools</option>
							<option>Paint</option>
							<option>Materials</option>
							<option>Others</option>
						</Form.Control>	
					</Form.Group>
					<Form.Group>
						<Form.Label>Stock:</Form.Label>
						<Form.Control type="number" value={stock} onChange={e => {setStock(e.target.value)}}  placeholder="Enter Number of Stocks" required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Price:</Form.Label>
						<Form.Control type="number" value={price} onChange={e => {setPrice(e.target.value)}}  placeholder="Enter Product Price" required/>
					</Form.Group>
					{
					isActive 
					? <Button variant="primary" type="submit">Add Product</Button>
					: <Button variant="secondary" disable>Add Product</Button>
					}
				</Form>
			</>	
		: <Redirect to="/login"/> 	

	)


}