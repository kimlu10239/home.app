import React,{useState,useEffect,useContext} from 'react';
import {Row,Col, Form, Button} from 'react-bootstrap';

import UserContext from '../userContext';
import {Redirect} from 'react-router-dom';

import Swal from 'sweetalert2';

export default function Login() {

	const {user,setUser} = useContext(UserContext);

	const [email,setEmail] = useState("");
	const [password,setPassword] = useState("");

	const [isActive,setIsActive] = useState(false);

	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[email,password])

	function loginUser(e){
		e.preventDefault();

		fetch('https://immense-brushlands-59734.herokuapp.com/users/login',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data=> {
			console.log(data);
			if(data.accessToken){

				Swal.fire({
				icon: "success",
				title: "Login Successful!",
				text: `Welcome!`
				})
				localStorage.setItem('token',data.accessToken)
				localStorage.setItem('email',email)


				fetch('https://immense-brushlands-59734.herokuapp.com/users/viewUser',{

					headers: {
						'Authorization': `Bearer ${data.accessToken}`
					}

				})
				.then(res => res.json())
				.then(data => {

					setUser({
						id: data._id,
						isAdmin: data.isAdmin
					})

				})	

			} else {
				Swal.fire({
					icon: "error",
					title: "Login Failed.",
					text: data.message
				})
			}

		})
	}	

	return (
		user.id
		? 
		user.isAdmin
			? <Redirect to="/products"/>
			: <Redirect to="/"/>		
		:
			<>
				<h1 className="my-5 text-center">Login</h1>
				<Row className="justify-content-md-center">
					<Col md={6}>	
						<Form onSubmit={e => loginUser(e)}>
							<Form.Group controlId="userEmail">
								<Form.Label>Email:</Form.Label>
								<Form.Control type="email" value={email} onChange={e => {setEmail(e.target.value)}} placeholder="Enter Email" required/>
							</Form.Group>
							<Form.Group controlId="userPassword">
								<Form.Label>Password:</Form.Label>
								<Form.Control type="password" value={password} onChange={e => {setPassword(e.target.value)}}  placeholder="Enter Password" required/>
							</Form.Group>

							{
							isActive 
							? <Button variant="primary" type="submit">Submit</Button>
							: <Button variant="secondary" disable>Submit</Button>
							}
						</Form>
					</Col>
				</Row>
			</>
	)	

}